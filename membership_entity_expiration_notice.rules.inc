<?php

/**
 * @file
 * Defines rules event to send mail.
 */

/**
 * Implements hook_rules_event_info().
 */
function membership_entity_expiration_notice_rules_event_info() {

  return array(
    'membership_entity_expiration_notice_membership_expiring' => array(
      'label' => t('Membership expiration notice'),
      'module' => 'membership_entity_expiration_notice',
      'group' => 'Membership',
      'variables' => array(
        'user_membership' => array(
          'label' => t('User membership.'),
          'type' => 'membership_entity',
        ),
        'expiration_offset' => array(
          'label' => t('Expiration offset(ex. 7 days).'),
          'type' => 'integer',
        ),
      ),
    ),
  );
}
