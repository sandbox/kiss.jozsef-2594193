CONTENTS OF THIS FILE
---------------------
  * Introduction
  * How to use
  * Dependencies

INTRODUCTION
------------
Current Maintainer: kiss.jozsef (József Kiss)
Sponsored by:
  * REEA (www.reea.net)

The Membership Entity Expiration Notice module provides a rules event to send
out notifications to user with expiring memberships and a custom table for logs.

  * Cron can be set when to check for expiring memberships.
  * The date offset can be set for expiring membership(ex. 14,30 days)

HOW TO USE
----------
  * Download the module and place it under 'sites/all/modules/contrib' folder
  * Enable the module from the modules page: 'admin/build/modules'
  * Set the cron rule and notification offset from the memberships page: 
  'admin/memberships/types/manage/membership'

DEPENDENCIES
------------
  * Elysia Cron (https://www.drupal.org/project/elysia_cron)
  * Membership Entity (https://www.drupal.org/project/membership_entity)
