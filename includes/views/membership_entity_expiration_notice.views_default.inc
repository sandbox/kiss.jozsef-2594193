<?php

/**
 * @file
 * Export of default views for memberships expiration notice.
 */

/**
 * Implements hook_views_default_views().
 */
function membership_entity_expiration_notice_views_default_views() {

  $views = array();

  $view = new view();
  $view->name = 'membership_entity_expiration_notice';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'membership_entity_expiration_notice';
  $view->human_name = 'Membership Entity Expiration Notice';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Membership entity expiration notice';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'administer memberships';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '30';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'table';
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'No expiration logs available.';
  $handler->display->display_options['empty']['area']['format'] = 'filtered_html';
  /* Relationship: Membership entity expiration notice: Membership Entity ID */
  $handler->display->display_options['relationships']['membership_id']['id'] = 'membership_id';
  $handler->display->display_options['relationships']['membership_id']['table'] = 'membership_entity_expiration_notice';
  $handler->display->display_options['relationships']['membership_id']['field'] = 'membership_id';
  /* Relationship: Membership entity expiration notice: Membership Entity Term ID */
  $handler->display->display_options['relationships']['membership_term_id']['id'] = 'membership_term_id';
  $handler->display->display_options['relationships']['membership_term_id']['table'] = 'membership_entity_expiration_notice';
  $handler->display->display_options['relationships']['membership_term_id']['field'] = 'membership_term_id';
  /* Relationship: Membership entity expiration notice: User ID */
  $handler->display->display_options['relationships']['user_id']['id'] = 'user_id';
  $handler->display->display_options['relationships']['user_id']['table'] = 'membership_entity_expiration_notice';
  $handler->display->display_options['relationships']['user_id']['field'] = 'user_id';
  /* Field: Membership entity expiration notice: Notice ID */
  $handler->display->display_options['fields']['id']['id'] = 'id';
  $handler->display->display_options['fields']['id']['table'] = 'membership_entity_expiration_notice';
  $handler->display->display_options['fields']['id']['field'] = 'id';
  /* Field: Membership: Rendered Membership */
  $handler->display->display_options['fields']['rendered_entity_1']['id'] = 'rendered_entity_1';
  $handler->display->display_options['fields']['rendered_entity_1']['table'] = 'views_entity_membership_entity';
  $handler->display->display_options['fields']['rendered_entity_1']['field'] = 'rendered_entity';
  $handler->display->display_options['fields']['rendered_entity_1']['relationship'] = 'membership_id';
  $handler->display->display_options['fields']['rendered_entity_1']['link_to_entity'] = 1;
  $handler->display->display_options['fields']['rendered_entity_1']['view_mode'] = 'full';
  $handler->display->display_options['fields']['rendered_entity_1']['bypass_access'] = 0;
  /* Field: Membership term: Rendered Membership term */
  $handler->display->display_options['fields']['rendered_entity']['id'] = 'rendered_entity';
  $handler->display->display_options['fields']['rendered_entity']['table'] = 'views_entity_membership_entity_term';
  $handler->display->display_options['fields']['rendered_entity']['field'] = 'rendered_entity';
  $handler->display->display_options['fields']['rendered_entity']['relationship'] = 'membership_term_id';
  $handler->display->display_options['fields']['rendered_entity']['link_to_entity'] = 1;
  $handler->display->display_options['fields']['rendered_entity']['view_mode'] = 'token';
  $handler->display->display_options['fields']['rendered_entity']['bypass_access'] = 0;
  /* Field: User: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'users';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['relationship'] = 'user_id';
  /* Field: Membership entity expiration notice: Expiration offset */
  $handler->display->display_options['fields']['expiration_offset']['id'] = 'expiration_offset';
  $handler->display->display_options['fields']['expiration_offset']['table'] = 'membership_entity_expiration_notice';
  $handler->display->display_options['fields']['expiration_offset']['field'] = 'expiration_offset';
  $handler->display->display_options['fields']['expiration_offset']['format_plural'] = TRUE;
  $handler->display->display_options['fields']['expiration_offset']['format_plural_singular'] = 'Membership will expire in 1 day';
  $handler->display->display_options['fields']['expiration_offset']['format_plural_plural'] = 'Membership will expire in @count days';
  /* Field: Membership entity expiration notice: Created */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'membership_entity_expiration_notice';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['date_format'] = 'long';
  $handler->display->display_options['fields']['created']['second_date_format'] = 'long';

  /* Display: Membership entity expiration notice */
  $handler = $view->new_display('page', 'Membership entity expiration notice', 'page');
  $handler->display->display_options['path'] = 'admin/memberships/log';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Expiration notice log';
  $handler->display->display_options['menu']['weight'] = '10';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;

  $views[$view->name] = $view;

  return $views;
}
