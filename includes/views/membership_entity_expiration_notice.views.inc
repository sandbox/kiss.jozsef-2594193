<?php

/**
 * @file
 * Display membership_entity_expiration_notice table data in views.
 */

/**
 * Implements hook_views_data().
 */
function membership_entity_expiration_notice_views_data() {

  $data = array();
  $data['membership_entity_expiration_notice']['table']['group'] = t('Membership entity expiration notice');
  $data['membership_entity_expiration_notice']['table']['base'] = array(
    'title' => t('Membership entity expiration notice'),
    'help' => t('Contains records we want exposed to Views.'),
  );
  $data['membership_entity_expiration_notice']['id'] = array(
    'title' => t('Notice ID'),
    'help' => t('Notice ID.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );
  $data['membership_entity_expiration_notice']['membership_id'] = array(
    'title' => t('Membership Entity ID'),
    'help' => t('Membership Entity ID.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'relationship' => array(
      'base' => 'membership_entity',
      'field' => 'membership_id',
      'handler' => 'views_handler_relationship',
      'label' => t('Membership entity'),
    ),
  );
  $data['membership_entity_expiration_notice']['membership_term_id'] = array(
    'title' => t('Membership Entity Term ID'),
    'help' => t('Membership Entity Term ID.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'relationship' => array(
      'base' => 'membership_entity_term',
      'field' => 'membership_term_id',
      'handler' => 'views_handler_relationship',
      'label' => t('Membership entity term'),
    ),
  );
  $data['membership_entity_expiration_notice']['user_id'] = array(
    'title' => t('User ID'),
    'help' => t('User ID.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'relationship' => array(
      'base' => 'users',
      'field' => 'user_id',
      'handler' => 'views_handler_relationship',
      'label' => t('User'),
    ),
  );
  $data['membership_entity_expiration_notice']['expiration_offset'] = array(
    'title' => t('Expiration offset'),
    'help' => t('Expiration offset.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );
  $data['membership_entity_expiration_notice']['created'] = array(
    'title' => t('Created'),
    'help' => t('Created.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  $data['membership_entity_expiration_notice']['table']['join'] = array(
    'membership_entity' => array(
      'left_field' => 'mid',
      'field' => 'membership_id',
    ),
  );
  $data['membership_entity_expiration_notice']['table']['join'] = array(
    'membership_entity_term' => array(
      'left_field' => 'id',
      'field' => 'membership_term_id',
    ),
  );
  $data['membership_entity_expiration_notice']['table']['join'] = array(
    'user' => array(
      'left_field' => 'uid',
      'field' => 'user_id',
    ),
  );

  return $data;
}
